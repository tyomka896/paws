/**
 * Generate unique ID
 */
function uniqueId(length = 4) {
    const symbols = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.split('')

    return Array(length).fill(0).map(() => {
        return symbols[Math.floor(Math.random() * symbols.length)]
    }).join('')
}

module.exports = {
    uniqueId: uniqueId,
}
