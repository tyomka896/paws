require('dotenv').config({ path: require('path').resolve('../.env') })
const WebSocket = require('ws')

const { uniqueId } = require('./uniqueId')

let players = []

const socket = new WebSocket.Server({
    path: '/ws',
    port: process.env.SOCKET_PORT,
})

socket.on('connection', ws => {
    ws.on('close', () => {
        players = players.filter(elem => elem.ws.readyState === elem.ws.OPEN)

        for (let player of players) {
            player.ws.send(JSON.stringify({
                t: 'status',
                c: players.length,
            }))
        }
    })

    ws.on('message', data => {
        data = JSON.parse(data)

        if (!data.t) return

        switch (data.t) {
            case 'paw':
                for (let player of players) {
                    if (player.userId === data.id) continue

                    player.ws.send(JSON.stringify({
                        t: 'paw',
                        uId: data.id,
                        pos: data.pos,
                    }))
                }

                break
            default:
                break
        }
    });

    const userId = uniqueId()

    players.push({ userId, ws })

    ws.send(JSON.stringify({
        t: 'enter',
        id: userId,
        c: players.length,
    }))

    for (let player of players) {
        if (player.userId === userId) continue

        player.ws.send(JSON.stringify({
            t: 'status',
            c: players.length,
        }))
    }
})
