import { pawDrawHandler } from './paw'
import { pawsElem, svgPawsElem, userInfo } from './variables'

/**
 * Mouse movement
 */
let MOUSE_CLICKED = false

window.addEventListener('click', pawDrawHandler);

[ 'mousedown', 'touchstart' ].forEach(elem => {
    pawsElem.addEventListener(elem, () => MOUSE_CLICKED = true)
});

[ 'mousemove', 'touchmove' ].forEach(elem =>
    window.addEventListener(elem, event => MOUSE_CLICKED && pawDrawHandler(event))
);

[ 'mouseup', 'touchend' ].forEach(elem =>
    window.addEventListener(elem, () => MOUSE_CLICKED = false)
);

[ 'focus', 'blur' ].forEach(elem =>
    window.addEventListener(elem, event => userInfo.focus = event.type === 'focus')
);

/**
 * SVG figures selection
 */
Array.from(document.querySelectorAll('.panel__icon'))
    .forEach((elem, index) => {
        const svg = svgPawsElem[index].cloneNode(true)
        svg.setAttribute('width', '100%')

        elem.querySelector('label').appendChild(svg)

        elem.addEventListener('click', event => {
            event.stopPropagation()

            const svgIndex = elem.getAttribute('data-index')

            if (!svgIndex) return

            userInfo.svg = parseInt(svgIndex)

            const ls = JSON.parse(localStorage.getItem('_info')) || {}
            ls.svg = svgIndex
            localStorage.setItem('_info', JSON.stringify(ls))
        })
    })

const ls = JSON.parse(localStorage.getItem('_info')) || {}
document.querySelectorAll('.panel__icon')[ls.svg || 0]
    .querySelector('input').click();
