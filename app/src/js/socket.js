import { drawPaw } from './paw'
import { userInfo, lastPawPos, countElem } from './variables'

export let socket = null

/**
 * WebSocket initialization
 */
if ('WebSocket' in window) {
    let _attempts = 3

    const protocol = window.location.protocol === 'http:' ? 'ws://' : 'wss://'
    const address = `${protocol}${window.location.hostname}/ws`

    const openHandler = () => {
        if (_attempts < 3) console.log('$ Reconnect is successful!')

        _attempts = 3
    }

    const messageHandler = event => {
        const data = JSON.parse(event.data)

        if (!data.t) return

        switch (data.t) {
            case 'enter':
                userInfo.id = data.id
                countElem.innerHTML = data.c

                break
            case 'status':
                countElem.innerHTML = data.c

                break
            case 'paw':
                // if (!userInfo.focus) break

                const { pos } = data

                if (window.innerWidth > pos.w) {
                    pos.x =  window.innerWidth / 2 + (pos.x - pos.w / 2)
                } else {
                    pos.x = window.innerWidth / 2 - (pos.w / 2 - pos.x)
                }

                if (window.innerHeight > pos.h) {
                    pos.y =  window.innerHeight / 2 + (pos.y - pos.h / 2)
                } else {
                    pos.y = window.innerHeight / 2 - (pos.h / 2 - pos.y)
                }

                drawPaw(pos.x, pos.y, {
                    size: 40,
                    theta: pos.th,
                    step: pos.s,
                    svg: pos.g,
                })

                break
        }
    }

    const closeHandler = () => {
        console.log('$ WebSocket connection was closed.')
        countElem.innerHTML = ''

        if (_attempts < 1) {
            console.log('$ No success to reconnect!')
            return
        }

        setTimeout(() => {
            console.log(`$ Trying to reconnect to WebSocket #${_attempts}. . .`)
            _attempts--
            createWebSocket()
        }, 5000)
    }

    const createWebSocket = () => {
        socket = new WebSocket(address)
        socket.addEventListener('open', openHandler)
        socket.addEventListener('message', messageHandler)
        socket.addEventListener('close', closeHandler)
    }

    createWebSocket()
}
else {
    alert('WebSockets not supported in your browser!')
}

/**
 * Send info of new paw drawn
 */
export function sendNewPaw(posX, posY, theta) {
    if (!socket || socket.readyState !== WebSocket.OPEN) return

    socket.send(JSON.stringify({
        t: 'paw',
        id: userInfo.id,
        pos: {
            x: Math.round(posX),
            y: Math.round(posY),
            w: window.innerWidth,
            h: window.innerHeight,
            th: Math.round(theta),
            s: lastPawPos.step,
            g: userInfo.svg,
        }
    }))
}
