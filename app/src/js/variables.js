/**
 * Basic element selections
 */
export const countElem = document.getElementById('count')

export const pawsElem = document.getElementById('paws')

export const svgPawsElem = document.getElementById('svg-icons').children

/**
 * Current user info
 */
export const userInfo = {
    id: '',
    color: '',
    svg: 0,
    focus: true,
}

/**
 * Last figure position info
 */
export const lastPawPos = {
    x: null,
    y: null,
    step: true,
}
