import { sendNewPaw } from './socket'
import { svgPawsElem, pawsElem, userInfo, lastPawPos } from './variables'

/**
 * Event handler for interaction of user
 */
export function pawDrawHandler(event) {
    const [ posX, posY ] = getMousePos(event)

    const isLastSet = lastPawPos.x && lastPawPos.y

    if (isLastSet) {
        const distance = getDistance(posX, posY, lastPawPos.x, lastPawPos.y)

        if (distance < 75) return
    }

    const theta = isLastSet ? getTheta(lastPawPos.x, lastPawPos.y, posX, posY) : 0

    lastPawPos.x = posX
    lastPawPos.y = posY
    lastPawPos.step = lastPawPos.step === 0 ? 1 : 0

    drawPaw(posX, posY, {
        theta: theta,
        step: lastPawPos.step,
        svg: userInfo.svg,
    })

    sendNewPaw(posX, posY, theta)
}

/**
 * Draw new paw on playground
 * @param {Number} x
 * @param {Number} y
 * @param {Object} options
 */
export function drawPaw(x, y, options) {
    if (!x || !y) return null

    if (x < 0) x = 0; else if (x > window.innerWidth) x = window.innerWidth
    if (y < 0) y = 0; else if (y > window.innerHeight) y = window.innerHeight

    options = Object.assign({
        size: 50,
        theta: 0,
        color: '#2A1205',
        step: 0,
        svg: 0,
    }, options)

    const elem = document.createElement('div')

    elem.style.position = 'absolute'
    elem.style.width = `${options.size}px`
    elem.style.height = `${options.size}px`
    elem.style.color = options.color
    elem.style.opacity = 1

    elem.appendChild(svgPawsElem[options.svg].cloneNode(true))

    const offsetX = x - options.size / 2
    const offsetY = y - options.size / 2

    elem.style.transform = `translate(${offsetX}px, ${offsetY}px)` +
        `rotate(${options.theta}deg)` +
        `scale(${Boolean(options.step) ? 1 : -1}, 1)`

    const _timer = setInterval(() => {
        let { opacity } = elem.style
        opacity -= .01

        if (opacity <= 0) {
            clearInterval(_timer)
            pawsElem.removeChild(elem)
        }
        else elem.style.opacity = opacity
    }, 10)

    pawsElem.appendChild(elem)

    if (pawsElem.children.length > 500) {
        pawsElem.removeChild(pawsElem.children[0])
    }

    return elem
}

/**
 * Angle between two points
 *
 * @param {Number} x1 X-axis of start point
 * @param {Number} y1 Y-axis of start point
 * @param {Number} x2 X-axis of end point
 * @param {Number} y2 Y-axis of end point
 */
export function getTheta(x1, y1, x2, y2) {
    const x = x2 - x1
    const y = y2 - y1

    if (x === 0) return y > 0 ? 180 : 0

    const theta = Math.atan(y / x) * 180 / Math.PI

    return x > 0 ? theta + 90 : theta + 270
}

/**
 * Distance between two points
 *
 * @param {Number} x1 X-axis of start point
 * @param {Number} y1 Y-axis of start point
 * @param {Number} x2 X-axis of end point
 * @param {Number} y2 Y-axis of end point
 */
export function getDistance(x1, y1, x2, y2) {
    return Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
}

/**
 * Mouse or touch position based on handler event
 *
 * @param {Object} event
 */
export function getMousePos(event) {
    if ([ 'touchstart', 'touchmove', 'touchend' ].includes(event.type)){
        const touch = event.touches[0] || event.changedTouches[0]

        return [ touch.pageX, touch.pageY ]
    } else if ([ 'click', 'mousedown', 'mousemove', 'mouseup' ].includes(event.type)) {
        return [ event.clientX, event.clientY ]
    }
    else return []
}
