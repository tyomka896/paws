import { defineConfig } from 'vite'
import legacy from '@vitejs/plugin-legacy'
import autoprefixer from 'autoprefixer'

export default defineConfig(({ command }) => {
    if (command === 'serve') return {
        root: 'src',
    }

    return {
        plugins: [
            legacy({ targets: [ 'ie >= 11' ] }),
        ],
        css: { postcss: { plugins: [ autoprefixer ] } },
        root: 'src',
        build: {
            outDir: '../public',
            emptyOutDir: true,
            rollupOptions: {
                output: {
                    entryFileNames: 'js/[name]-[hash].js',
                    chunkFileNames: '_/[name]-[hash].js',
                    assetFileNames: ({ name }) => {
                        if (/\.(gif|jpe?g|png|svg)$/.test(name ?? ''))
                            return 'img/[name]-[hash][extname]'
                        else if (/\.css$/.test(name ?? ''))
                            return 'css/[name]-[hash][extname]'
                        else if (/\.ico$/.test(name ?? ''))
                            return '[name]-[hash][extname]'
                        else return 'assets/[name]-[hash][extname]'
                    },
                }
            }
        },
    }
})
